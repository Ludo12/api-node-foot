const Matchs = require('../models/matchPopulate.models');
const slugify = require('slugify');

const Poules = require('../models/poulePopulate.models');
const Teams = require('../models/team.models');
const Resultats = require('../models/resultat.models');

function sav(matchList, nameP) {
    matchList.forEach((team) => {
        setTimeout(async () => {
            let team1 = await Teams.findOne({
                name: team[0]
            });
            let team2 = await Teams.findOne({
                name: team[1]
            });

            let match = await Matchs.create({
                name: `${team1.name}` + ' - ' + `${team2.name}`,
                slug: `${team1.slug}` + '-' + `${team2.slug}`,
                pouleName: nameP,
            });

            await match.score.push({
                'name': team1
            });

            await match.score.push({
                'name': team2
            });

            await match.save();
        }, 2000);
    })
};

let utils = {
    getMatchs: function (arrayOfTeams, back = false) {
        let arrMatchs = {};
        for (let index = 0; index < arrayOfTeams.length; index++) {
            //nombre d'equipe dans la poule
            let nbteams = arrayOfTeams[index].resultats.length;

            //le nom de la poule
            let namePoule = arrayOfTeams[index].name;

            //tableau des noms d'equipe presente dans la poule
            let teamName = [];

            arrayOfTeams[index].resultats.forEach(x => {
                teamName.push(x.team.name);
            });

            //si matchs aller-retour
            if (back) {
                teamName.reverse()
            }
            //creer les matchs selon le nombre d'equipe dans la poule
            switch (nbteams) {
                case 2:
                    arrMatchs[namePoule] = [
                        [teamName[0], teamName[1]]
                    ];
                    break;
                case 3:
                    arrMatchs[namePoule] = [
                        [teamName[0], teamName[1]],
                        [teamName[1], teamName[2]],
                        [teamName[0], teamName[2]]
                    ];
                    break;
                case 4:
                    arrMatchs[namePoule] = [
                        [teamName[0], teamName[1]],
                        [teamName[2], teamName[3]],
                        [teamName[0], teamName[2]],
                        [teamName[1], teamName[3]],
                        [teamName[0], teamName[3]],
                        [teamName[1], teamName[2]]
                    ];
                    break;
                case 5:
                    arrMatchs[namePoule] = [
                        [teamName[0], teamName[1]],
                        [teamName[2], teamName[3]],
                        [teamName[4], teamName[0]],
                        [teamName[1], teamName[2]],
                        [teamName[3], teamName[0]],
                        [teamName[2], teamName[4]],
                        [teamName[1], teamName[3]],
                        [teamName[4], teamName[1]],
                        [teamName[0], teamName[2]],
                        [teamName[3], teamName[4]]
                    ];
                    break;
                case 6:
                    arrMatchs[namePoule] = [
                        [teamName[0], teamName[1]],
                        [teamName[2], teamName[3]],
                        [teamName[4], teamName[5]],
                        [teamName[0], teamName[2]],
                        [teamName[1], teamName[5]],
                        [teamName[3], teamName[4]],
                        [teamName[0], teamName[3]],
                        [teamName[1], teamName[2]],
                        [teamName[4], teamName[1]],
                        [teamName[5], teamName[3]],
                        [teamName[0], teamName[4]],
                        [teamName[2], teamName[5]],
                        [teamName[1], teamName[3]],
                        [teamName[0], teamName[5]],
                        [teamName[4], teamName[2]]
                    ];
                    break;
                case 7:
                    arrMatchs[namePoule] = [
                        [teamName[0], teamName[1]],
                        [teamName[2], teamName[3]],
                        [teamName[4], teamName[5]],
                        [teamName[6], teamName[0]],
                        [teamName[1], teamName[2]],
                        [teamName[3], teamName[4]],
                        [teamName[5], teamName[6]],
                        [teamName[0], teamName[2]],
                        [teamName[1], teamName[3]],
                        [teamName[4], teamName[6]],
                        [teamName[5], teamName[0]],
                        [teamName[1], teamName[4]],
                        [teamName[2], teamName[5]],
                        [teamName[3], teamName[6]],
                        [teamName[4], teamName[0]],
                        [teamName[1], teamName[5]],
                        [teamName[2], teamName[6]],
                        [teamName[3], teamName[5]],
                        [teamName[6], teamName[1]],
                        [teamName[4], teamName[2]],
                        [teamName[0], teamName[3]]
                    ];
                    break;
                case 8:
                    arrMatchs[namePoule] = [
                        [teamName[0], teamName[1]],
                        [teamName[2], teamName[3]],
                        [teamName[4], teamName[5]],
                        [teamName[6], teamName[7]],
                        [teamName[1], teamName[2]],
                        [teamName[3], teamName[4]],
                        [teamName[5], teamName[6]],
                        [teamName[7], teamName[0]],
                        [teamName[1], teamName[3]],
                        [teamName[4], teamName[6]],
                        [teamName[0], teamName[2]],
                        [teamName[5], teamName[7]],
                        [teamName[4], teamName[1]],
                        [teamName[5], teamName[2]],
                        [teamName[3], teamName[7]],
                        [teamName[6], teamName[0]],
                        [teamName[2], teamName[6]],
                        [teamName[3], teamName[5]],
                        [teamName[0], teamName[4]],
                        [teamName[1], teamName[7]],
                        [teamName[2], teamName[4]],
                        [teamName[1], teamName[5]],
                        [teamName[4], teamName[7]],
                        [teamName[3], teamName[0]],
                        [teamName[3], teamName[6]],
                        [teamName[0], teamName[5]],
                        [teamName[7], teamName[2]],
                        [teamName[1], teamName[6]]
                    ];
                    break;
            }
        };

        return arrMatchs
    },
    saveMatchs: async function (obj, back = false) {
        if (back) {
            for (let match in obj['normal']) {
                sav(obj['normal'][match], match)
            };
            for (let match in obj['reversed']) {
                sav(obj['reversed'][match], match)
            }
        } else {
            for (let pouleName in obj) {
                sav(obj[pouleName], pouleName)
            };
        }
    },
    resultats: async (namePoule, nameTeam1, nameTeam2, req, res) => {
        //on ajoute les pts pour la poule du match
        //on initialise les resultats globals avant de faire le total des points
        let resultatTeam1 = {
            "pts": 0,
            "bc": 0,
            "bp": 0
        };
        let resultatTeam2 = {
            "pts": 0,
            "bc": 0,
            "bp": 0
        };

        //on recupere tous les matchs de la poule qui ont été joué
        let many = await Matchs.find({
            pouleName: namePoule,
            played: true
        }).populate('score.name');


        let resultatPoule = await Resultats.find({
            poule: namePoule
        }).populate('team');

        //on comptabilise tous les points des equipes
        many.map(el => {
            if (el.slug === req.params.matchSlug) {
                if (el.score[0].but > el.score[1].but) {
                    resultatTeam1.pts = 3;
                    resultatTeam2.pts = 0;
                } else if (el.score[0].but < el.score[1].but) {
                    resultatTeam1.pts = 0;
                    resultatTeam2.pts = 3;
                } else if (el.score[0].but === el.score[1].but) {
                    resultatTeam1.pts = 1;
                    resultatTeam2.pts = 1;
                }

                el.score.map(x => {
                    if (x.name.name === nameTeam1) {
                        resultatTeam1['bp'] += x.but;
                        resultatTeam2['bc'] += x.but;
                    }
                    if (x.name.name === nameTeam2) {
                        resultatTeam2['bp'] += x.but;
                        resultatTeam1['bc'] += x.but;
                    }
                })
            }
        })
        try {
            //on ajoute les points et les buts dans les resultats puis on sauvegarde
            resultatPoule.forEach(teamResultat => {
                setTimeout(() => {
                    if (teamResultat.team.name === nameTeam1) {
                        teamResultat['pts'] += resultatTeam1['pts'];
                        teamResultat['bp'] += resultatTeam1['bp'];
                        teamResultat['bc'] += resultatTeam1['bc'];
                        teamResultat.save()
                            .then((data) => console.log('success'))
                            .catch(err => {
                                res.status(500).send({
                                    message: err.message || "Erreur dans l'affichage des matchs'"
                                });
                            });
                    }
                }, 1000);
                setTimeout(() => {
                    if (teamResultat.team.name === nameTeam2) {
                        teamResultat['pts'] += resultatTeam2['pts'];
                        teamResultat['bp'] += resultatTeam2['bp'];
                        teamResultat['bc'] += resultatTeam2['bc']
                        teamResultat.save()
                            .then((data) => console.log('success'))
                            .catch(err => {
                                res.status(500).send({
                                    message: err.message || "Erreur dans l'affichage des matchs'"
                                });
                            });
                    }
                }, 1000);

            });
        } catch (error) {
            res.json({
                'message': error.message
            })
        }
    },
    factoriel: function (num) {
        if (num == 0) {
            return 1;
        } else {
            return num * fact(num - 1);
        }
    }
}
module.exports = utils