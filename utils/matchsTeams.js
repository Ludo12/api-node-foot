const Matchs = require('../models/match.models');
const slugify = require('slugify');

const Poules = require('../models/poule.models');
async function sav(matchList, matchOfPoule) {
    await matchList.forEach(async (team) => {

        let team1 = team[0].toString();
        let team2 = team[1].toString();


        setTimeout(() => {
            let match = new Matchs({
                name: `${team1}` + ' - ' + `${team2}`,
                slug: slugify(`${team1}` + ' - ' + `${team2}`, {
                    lower: true
                }),
                pouleName: matchOfPoule,
                team1: team1,
                team2: team2
            });

            match.score.push({
                'name': team1
            });

            match.score.push({
                'name': team2
            });

            match.resultat.push({
                'name': team1
            });

            match.resultat.push({
                'name': team2
            });
            match.save();
        }, 1000);
    })
};

let utils = {
    getMatchs: function (arrayOfTeams, back = false) {
        let arrMatchs = {};
        for (let index = 0; index < arrayOfTeams.length; index++) {
            //nombre d'equipe dans la poule
            let nbteams = arrayOfTeams[index].resultats.length;

            //le nom de la poule
            let namePoule = arrayOfTeams[index].name;

            //tableau des noms d'equipe presente dans la poule
            let teamName = [];
            arrayOfTeams[index].resultats.forEach(x => {
                teamName.push(x.name);
            });

            //si matchs aller-retour
            if (back) {
                teamName.reverse()
            }

            //creer les matchs selon le nombre d'equipe dans la poule
            switch (nbteams) {
                case 3:
                    arrMatchs[namePoule] = [
                        [teamName[0], teamName[1]],
                        [teamName[1], teamName[2]],
                        [teamName[0], teamName[2]]
                    ];
                    break;
                case 4:
                    arrMatchs[namePoule] = [
                        [teamName[0], teamName[1]],
                        [teamName[2], teamName[3]],
                        [teamName[0], teamName[2]],
                        [teamName[1], teamName[3]],
                        [teamName[0], teamName[3]],
                        [teamName[1], teamName[2]]
                    ];
                    break;
                case 5:
                    arrMatchs[namePoule] = [
                        [teamName[0], teamName[1]],
                        [teamName[2], teamName[3]],
                        [teamName[4], teamName[0]],
                        [teamName[1], teamName[2]],
                        [teamName[3], teamName[0]],
                        [teamName[2], teamName[4]],
                        [teamName[1], teamName[3]],
                        [teamName[4], teamName[1]],
                        [teamName[0], teamName[2]],
                        [teamName[3], teamName[4]]
                    ];
                    break;
                case 6:
                    arrMatchs[namePoule] = [
                        [teamName[0], teamName[1]],
                        [teamName[2], teamName[3]],
                        [teamName[4], teamName[5]],
                        [teamName[0], teamName[2]],
                        [teamName[1], teamName[5]],
                        [teamName[3], teamName[4]],
                        [teamName[0], teamName[3]],
                        [teamName[1], teamName[2]],
                        [teamName[4], teamName[1]],
                        [teamName[5], teamName[3]],
                        [teamName[0], teamName[4]],
                        [teamName[2], teamName[5]],
                        [teamName[1], teamName[3]],
                        [teamName[0], teamName[5]],
                        [teamName[4], teamName[2]]
                    ];
                    break;
                case 7:
                    arrMatchs[namePoule] = [
                        [teamName[0], teamName[1]],
                        [teamName[2], teamName[3]],
                        [teamName[4], teamName[5]],
                        [teamName[6], teamName[0]],
                        [teamName[1], teamName[2]],
                        [teamName[3], teamName[4]],
                        [teamName[5], teamName[6]],
                        [teamName[0], teamName[2]],
                        [teamName[1], teamName[3]],
                        [teamName[4], teamName[6]],
                        [teamName[5], teamName[0]],
                        [teamName[1], teamName[4]],
                        [teamName[2], teamName[5]],
                        [teamName[3], teamName[6]],
                        [teamName[4], teamName[0]],
                        [teamName[1], teamName[5]],
                        [teamName[2], teamName[6]],
                        [teamName[3], teamName[5]],
                        [teamName[6], teamName[1]],
                        [teamName[4], teamName[2]],
                        [teamName[0], teamName[3]]
                    ];
                    break;
                case 8:
                    arrMatchs[namePoule] = [
                        [teamName[0], teamName[1]],
                        [teamName[2], teamName[3]],
                        [teamName[4], teamName[5]],
                        [teamName[6], teamName[7]],
                        [teamName[1], teamName[2]],
                        [teamName[3], teamName[4]],
                        [teamName[5], teamName[6]],
                        [teamName[7], teamName[0]],
                        [teamName[1], teamName[3]],
                        [teamName[4], teamName[6]],
                        [teamName[0], teamName[2]],
                        [teamName[5], teamName[7]],
                        [teamName[4], teamName[1]],
                        [teamName[5], teamName[2]],
                        [teamName[3], teamName[7]],
                        [teamName[6], teamName[0]],
                        [teamName[2], teamName[6]],
                        [teamName[3], teamName[5]],
                        [teamName[0], teamName[4]],
                        [teamName[1], teamName[7]],
                        [teamName[2], teamName[4]],
                        [teamName[1], teamName[5]],
                        [teamName[4], teamName[7]],
                        [teamName[3], teamName[0]],
                        [teamName[3], teamName[6]],
                        [teamName[0], teamName[5]],
                        [teamName[7], teamName[2]],
                        [teamName[1], teamName[6]]
                    ];
                    break;
            }
        };

        return arrMatchs
    },
    saveMatchs: async function (obj, back = false) {
        if (back) {
            for (let match in obj['normal']) {
                sav(obj['normal'][match], match)
            };
            for (let match in obj['reversed']) {
                sav(obj['reversed'][match], match)
            }
        } else {
            for (let pouleName in obj) {
                sav(obj[pouleName], pouleName)
            };
        }
    },
    resultats: async (namePoule, nameTeam1, nameTeam2, req, res) => {
        //on ajoute les pts pour la poule du match
        //on initialise les resultats globals avant de faire le total des points
        let resultatTeam1 = {
            "pts": 0,
            "bc": 0,
            "bp": 0
        };
        let resultatTeam2 = {
            "pts": 0,
            "bc": 0,
            "bp": 0
        };

        //on recupere tous les matchs de la poule qui ont été joué
        let many = await Matchs.find({
            pouleName: namePoule,
            played: true
        }, 'resultat score slug');

        //on comptabilise tous les points des equipes
        many.map(el => {
            if (el.slug === req.params.matchSlug) {
                el.resultat.map(x => {
                    if (x.name === nameTeam1) {
                        resultatTeam1['pts'] += x.pts;
                    }
                    if (x.name === nameTeam2) {
                        resultatTeam2['pts'] += x.pts;
                    }
                })
                el.score.map(x => {
                    if (x.name === nameTeam1) {
                        resultatTeam1['bp'] += x.but;
                        resultatTeam2['bc'] += x.but;
                    }
                    if (x.name === nameTeam2) {
                        resultatTeam2['bp'] += x.but;
                        resultatTeam1['bc'] += x.but;
                    }
                })
            }
        })

        //on ajoute les points à la poule
        let poule = await Poules.findOne({
            name: namePoule
        }, 'resultats');

        poule.resultats.forEach(x => {
            if (x.name === nameTeam1) {
                x['pts'] += resultatTeam1['pts'];
                x['bp'] += resultatTeam1['bp'];
                x['bc'] += resultatTeam1['bc'];
            }
            if (x.name === nameTeam2) {
                x['pts'] += resultatTeam2['pts'];
                x['bp'] += resultatTeam2['bp'];
                x['bc'] += resultatTeam2['bc']
            }
        });

        //on update la poule en question
        await Poules.findOneAndUpdate({
            name: namePoule
        }, poule, {
            new: true,
            runValidators: true
        }).then((data) => console.log('success')).catch(err => {
            res.status(500).send({
                message: err.message || "Erreur dans l'affichage des matchs'"
            });
        });

    },
    factoriel: function (num) {
        if (num == 0) {
            return 1;
        } else {
            return num * fact(num - 1);
        }
    }
}
module.exports = utils