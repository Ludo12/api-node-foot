const mongoose = require('mongoose');
const dbConfig = require('./db');
const connectDB = async () => {
    return await mongoose
        .connect(`mongodb://${dbConfig.HOST}:${dbConfig.PORT}/${dbConfig.DB}`, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        })
        .then(() => console.log("DB connect ok"))
        .catch(err => {
            console.error("Connection error", err);
            process.exit();
        })
}

module.exports = connectDB