const express = require('express');
const route = express.Router();
const slugify = require('slugify');

const Matchs = require('../models/match.models');
const Poules = require('../models/poule.models');
const Teams = require('../models/team.models');

const {
    getMatchs,
    saveMatchs,
    resultats
} = require('../utils/matchsTeams');

const {
    body,
    param,
    validationResult
} = require('express-validator');

route.get('/gen/', async (req, res) => {
    try {
        const allTeamInPoule = await Poules
            .find({}, 'name resultats');

        let objMatchs = {};

        if (req.tournoi.back) {
            objMatchs['normal'] = getMatchs(allTeamInPoule);
            objMatchs['reversed'] = getMatchs(allTeamInPoule, req.tournoi.back);
        } else {
            objMatchs = getMatchs(allTeamInPoule);
        }
        saveMatchs(objMatchs, req.tournoi.back)

        res.sendStatus(200);
    } catch (error) {
        res.json({
            message: error.message
        })
    };
});

//toutes les matchs
route.get('/all', async (req, res) => {
    await Matchs.find().sort({
        "createdAt": 1
    }).then(data => res.status(200).send(data)).catch(err => {
        res.status(500).send({
            message: err.message || "Erreur dans l'affichage des matchs'"
        });
    });
});

//supprime tous les matchs
route.delete('/deleteall', async (req, res) => {
    await Matchs.deleteMany({})
        .then(data => {
            res.send({
                mesaage: `${data.deletedCount} Matchs ont été supprimés avec succes !`
            });
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Une erreur c'est produite supprimer les Mathcs"
            })
        })
});

//update de l'équipe
route.put('/update/:matchSlug', //vérification avec le middleware body
    body('scoreTeam1').if(body('scoreTeam1').notEmpty()).trim().escape().isNumeric().withMessage('Vous devez entrer un nombre'),
    body('scoreTeam2').if(body('scoreTeam2').notEmpty()).trim().escape().isNumeric().withMessage('Vous devez entrer un nombre'),
    async (req, res) => {
        try {
            //Array des erreurs
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array()
                });
            }

            let pts = {};

            //on cherche le match par le slug
            let teamMatch = await Matchs.findOne({
                    slug: req.params.matchSlug
                }).then(data => {
                    if (data === 'undefined' || data === null) {
                        res.status(404).send({
                            message: "Le match n'existe pas"
                        })
                    }
                    return data
                })
                .catch(err => {
                    res.status(500).send({
                        message: "Erreur match"
                    })
                });

            //on verifi si le match existe
            if (teamMatch !== (null && 'undefined')) {
                //on attribut les points aux equipes en fonction du score

                for (const key in teamMatch.score) {
                    if (teamMatch.score[key].name === teamMatch.team1) {
                        teamMatch.score[key]['but'] = Number(req.body.scoreTeam1);
                    }
                    if (teamMatch.score[key].name === teamMatch.team2) {
                        teamMatch.score[key]['but'] = Number(req.body.scoreTeam2);
                    }
                };

                if (Number(req.body.scoreTeam1) > Number(req.body.scoreTeam2)) {
                    pts[teamMatch.team1] = 3;
                    pts[teamMatch.team2] = 0;
                } else if (Number(req.body.scoreTeam1) < Number(req.body.scoreTeam2)) {
                    pts[teamMatch.team1] = 0;
                    pts[teamMatch.team2] = 3;
                } else if (Number(req.body.scoreTeam1) === Number(req.body.scoreTeam2)) {
                    pts[teamMatch.team1] = 1;
                    pts[teamMatch.team2] = 1;
                }

                for (const key in teamMatch.resultat) {
                    if (teamMatch.resultat[key].name === teamMatch.team1) {
                        teamMatch.resultat[key]['pts'] = pts[teamMatch.team1];
                    }
                    if (teamMatch.resultat[key].name === teamMatch.team2) {
                        teamMatch.resultat[key]['pts'] = pts[teamMatch.team2];
                    }
                };

                teamMatch.played = true;
                await teamMatch.save()
                    .then((data) => {
                        res.status(200).send({
                            message: 'update scrore with success'
                        })
                    }).catch(err => {
                        res.status(500).send({
                            message: 'error update score'
                        })
                    });

                //on update le score et les pts du match
                // await Matchs.findOneAndUpdate({
                //     slug: req.params.matchSlug
                // }, {
                //     $set: {
                //         score: score,
                //         pts: pts,
                //         played: true
                //     }
                // }).then((data) => {
                //     res.status(200).send({
                //         message: 'update scrore with success'
                //     })
                // }).catch(err => {
                //     res.status(500).send({
                //         message: 'error update score'
                //     })
                // });

                //on met à jour le resultat pts,score dans la poule
                resultats(teamMatch.pouleName, teamMatch.team1, teamMatch.team2, req, res);
            }

        } catch (error) {
            res.json({
                message: error.message || "Error match"
            })
        }
    });

route.get('/resultat/:pouleSlug',
    param('pouleSlug').trim().escape().isString(),
    async (req, res) => {
        let tri = await Poules.aggregate([{
                $match: {
                    "slug": req.params.pouleSlug
                }
            }, {
                $unwind: "$resultats"
            },
            {
                $sort: {
                    "resultats.pts": -1,
                    "resultats.bp": -1,
                    "resultats.bc": 1,
                }
            },
            {
                $group: {
                    _id: "$_id",
                    resultats: {
                        $push: "$resultats"
                    }
                }
            }
        ]);
        try {
            res.json(tri)
        } catch (error) {
            res.json({
                message: error
            })
        }
    });

module.exports = route