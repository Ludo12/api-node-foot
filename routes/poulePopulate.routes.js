const express = require('express');
const route = express.Router();
const slugify = require('slugify');

const Poules = require('../models/poulePopulate.models');
const Teams = require('../models/team.models');
const Resultats = require('../models/resultat.models');

const {
    body,
    param,
    validationResult
} = require('express-validator');

//créer une poule
route.post('/create', //vérification avec le middleware 
    body('name').not().isEmpty().trim().escape().isString().withMessage("Le nom doit être renseigné"),
    async (req, res) => {
        try {
            //Array des erreurs
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array()
                });
            }
            //Vérification si le nom de la poule est déjà enregistré
            const pouleExist = await Poules.find({
                name: req.body.name
            }).countDocuments() > 0 ? true : false;

            //si la poule existe on indique une erreur
            if (pouleExist) {
                res.status(400).send({
                    message: `La poule ${req.body.name} existe déjà`
                });
                return;
            } else { //sinon on crée la poule
                const poule = new Poules({
                    name: req.body.name,
                    tournoi_id: req.tournoi,
                    slug: slugify(req.body.name, {
                        lower: true
                    })
                });

                await poule
                    .save(poule)
                    .then(data => {
                        res.status(200).send(data);
                    })
                    .catch(err => {
                        res.status(500).send({
                            message: err.message || "Erreur dans la creation de la poule"
                        });
                    });

                tournoi = req.tournoi;
                tournoi.nbPoule = await Poules.find().countDocuments();
                await tournoi.save(tournoi);
            };
        } catch (error) {
            res.json({
                message: error
            })
        }
    });

//toutes les poules
route.get('/all', async (req, res) => {
    await Poules.find().populate({
        path: 'resultats', //populate resultats
        options: {
            sort: { //tri
                'pts': -1,
                'bp': -1,
                'bc': 1
            }
        },
        populate: {
            path: "team", //in resultats, populate team
            select: 'name'
        }
    }).then(data => res.status(200).send(data)).catch(err => {
        res.status(500).send({
            message: err.message || "Erreur dans la recherche des poules"
        });
    });
});

//ajout des équipes à la poule
route.post('/add/:pouleSlug/:teamSlug',
    param('pouleSlug').trim().escape().isString(),
    param('teamSlug').trim().escape().isString(),
    async (req, res) => {
        //Array des erreurs
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({
                errors: errors.array()
            });
        }

        //on vérifie si la poule existe si > 0 exist
        const testPoule = await Poules.findOne({
            slug: req.params.pouleSlug
        }).countDocuments() > 0 ? true : false;

        //on vérifie si l'équipe existe si > 0 exist
        const testTeam = await Teams.findOne({
            slug: req.params.teamSlug
        }).countDocuments() > 0 ? true : false;

        //si faux message d'erreur
        if (!testPoule) {
            res.status(400).send({
                message: `Erreur la poule n'existe pas`
            });
            return;
        } else if (!testTeam) {
            res.status(400).send({
                message: `Erreur l'equipe n'existe pas`
            });
            return;
        }

        //on selectionne la poule
        const poule = await Poules.findOne({
            slug: req.params.pouleSlug
        });

        //on selectionne l'equipe
        const team = await Teams.findOne({
            slug: req.params.teamSlug
        });

        //on vérifie si on a deja ajouté cette équipe si > 0 deja ajouté
        const testResultat = await Poules.find({
            'resultats': team
        }).countDocuments() > 0 ? true : false;

        //si vrai message d'erreur
        if (testResultat) {
            res.status(400).send({
                message: `Erreur l'equipe ${team.name} est déjà dans une poule`
            });
            return;
        }

        //on crée les resultats pour les teams en fonction de la poule
        const resultat = new Resultats({
            poule: poule.name,
            tournoi_id: req.tournoi,
            team: team
        });
        await resultat.save();

        //on initialise les resultats dans la poule
        poule.resultats.push(resultat);

        const savedPoule = await poule
            .save(poule)
            .then(data => {
                res.status(200).send(data);
            })
            .catch(err => {
                res.status(500).send({
                    message: err.message || "Erreur dans l'ajout de l'équipe à la poule"
                });
            });

        try {
            res.json(savedPoule)
        } catch (error) {
            res.json({
                message: error.message
            })
        }
    })

//supprime tous les matchs de la poule
route.post('/deleteall/:pouleSlug', async (req, res) => {
    const deleteTeam = await Poules.findOne({
            slug: req.params.pouleSlug
        }).then()
        .catch(err => {
            res.status(500).send({
                message: "Erreur dans la suppression de l'équipe"
            });
        });
    deleteTeam.resultats = []
    deleteTeam.save();

    await Resultats.deleteMany({
        poule: deleteTeam.name
    });
    res.json({
        deleteTeam,
        message: "Equipe supprimée de la poule"
    })
});

//on supprime une poule
route.delete('/delete/:pouleSlug',
    param('pouleSlug').trim().escape().isString(),
    async (req, res) => {
        try {
            //Array des erreurs
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array()
                });
            }

            //on recherche par le slug et on supprime
            const removedPoule = await Poules.findOneAndDelete({
                slug: req.params.pouleSlug
            })
            res.json({
                removedPoule
            })
        } catch (error) {
            res.json({
                message: "La poule n'a pas pu être supprimée"
            })
        }
    });

//on supprime une équipe d'une poule spécifique
route.post('/one/:pouleId/:teamSlug',
    param('pouleId').trim().escape().isString(),
    param('teamSlug').trim().escape().isString(),
    async (req, res) => {
        //Array des erreurs
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({
                errors: errors.array()
            });
        }

        //on cherche la poule
        const deleteTeam = await Poules.findOne({
            '_id': req.params.pouleId
        }).populate({
            path: "resultats", // populate resultats
            populate: {
                path: "team", //in resultats, populate team
                model: 'Team'
            }
        });

        //on supprime l'equipe de la poule
        deleteTeam.resultats.forEach(el => {
            if (el.team.slug === req.params.teamSlug) {
                deleteTeam.resultats.remove(el);
            };
        })

        //on sauvegarde la modification
        deleteTeam.save()
            .then()
            .catch(err => {
                res.status(500).send({
                    message: "Erreur dans la suppression de l'équipe"
                });
            });

        res.json({
            deleteTeam,
            message: "Equipe supprimée de la poule"
        })
    });

//recherche d'une equipe specifiques dans une poule donnée
route.get('/search/:pouleSlug/:teamSlug',
    param('pouleSlug').trim().escape().isString(),
    param('teamSlug').trim().escape().isString(),
    async (req, res) => {
        //Array des erreurs
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({
                errors: errors.array()
            });
        }

        const nameTeam = (req.params.teamSlug.charAt(0).toUpperCase() + req.params.teamSlug.slice(1).replace('-', ' '));
        const namePoule = (req.params.pouleSlug.charAt(0).toUpperCase() + req.params.pouleSlug.slice(1).replace('-', ' '));

        //on vérifie si l'équipe choisit est déjà dans une poule precise
        const poule = await Poules.findOne({
            slug: req.params.pouleSlug
        }).populate({
            path: "resultats", // populate resultats
            populate: {
                path: "team", //in resultats, populate team
                select: "slug",
                model: 'Team'
            }
        }).then().catch((err) => {
            res.status(400).send({
                message: err.message
            });
        });

        if (poule === null) {
            res.status(400).send({
                message: `La poule : ${namePoule} n'existe pas`
            });
        } else {
            let presente = false;
            poule.resultats.map(x => {
                if (x.team.slug === req.params.teamSlug) {
                    presente = true
                };
            });

            if (presente) {
                res.status(400).send({
                    message: `L'equipe ${nameTeam} est bien dans la poule : ${poule.name}`
                });
            } else {
                res.status(400).send({
                    message: `L'equipe  ${nameTeam} n'est pas présente dans la poule : ${poule.name} ou n'existe pas`
                });
            }
        }

    });

module.exports = route