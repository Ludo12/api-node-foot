const express = require('express');
const route = express.Router();
const slugify = require('slugify');

const Tournois = require('../models/tournoi.models');
const Poules = require('../models/poule.models');
const Teams = require('../models/team.models');

const {
    body,
    param,
    validationResult
} = require('express-validator');

//créer un tournoi
route.post('/create', //vérification avec le middleware 
    body('name').not().isEmpty().trim().escape().withMessage("Le nom du tournoi doit être renseigné"),
    body('date').if(body('date').notEmpty()).trim().escape().isDate().withMessage("Date incorect"),
    async (req, res) => {
        try {
            //Array des erreurs
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array()
                });
            }
            //Vérification si le nom de la poule est déjà enregistré
            const tournoiExist = await Tournois.find({
                name: req.body.name
            }).countDocuments() > 0 ? true : false;

            //si la poule existe on indique une erreur
            if (tournoiExist) {
                res.status(400).send({
                    message: `La tournoi ${req.body.name} existe déjà`
                });
                return;
            } else { //sinon on crée la tournoi

                const tournoi = new Tournois({
                    name: req.body.name,
                    date: req.body.date,
                    nbPoule: await Poules.find().countDocuments(),
                    nbTeam: await Teams.find().countDocuments(),
                    slug: slugify(req.body.name, {
                        lower: true
                    })
                });

                await tournoi
                    .save(tournoi)
                    .then(data => {
                        res.status(200).send(data);
                    })
                    .catch(err => {
                        res.status(500).send({
                            message: err.message || "Erreur dans la creation de la tournoi"
                        });
                    });
            };
        } catch (error) {
            res.json({
                message: error
            })
        }
    }
);

//toutes les tournois
route.get('/all', async (req, res) => {
    await Tournois.find().then(data => res.status(200).send(data)).catch(err => {
        res.status(500).send({
            message: err.message || "Erreur dans la recherche des tournois"
        });
    });
});

//on supprime un tournoi
route.delete('/delete/:tournoiId',
    param('tournoiId').trim().escape().isString(),
    async (req, res) => {
        try {
            //Array des erreurs
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array()
                });
            }

            //on recherche par l'id et on supprime
            const removedTournoi = await Tournois.findByIdAndDelete({
                _id: req.params.tournoiId
            })
            res.json({
                removedTournoi
            })
        } catch (error) {
            res.json({
                message: error
            })
        }
    });

//update tournoi
route.put('/update/:tournoiSlug', //vérification avec le middleware body
    body('name').if(body('name').notEmpty()).trim().escape().isString().withMessage("Nom incorrect"),
    body('date').if(body('date').notEmpty()).trim().escape().isDate().withMessage("Date incorrect"),
    async (req, res) => {
        try {
            //Array des erreurs
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array()
                });
            }

            req.body.slug = slugify(req.body.name, {
                lower: true
            });
            req.body.nbPoule = await Poules.find().countDocuments();
            req.body.nbTeam = await Teams.find().countDocuments();


            //on recherche par l'id et on fait les modifications 
            const updatedTournoi = await Tournois.findOneAndUpdate({
                slug: req.params.tournoiSlug
            }, req.body, {
                new: true,
                runValidators: true
            });
            res.json({
                updatedTournoi
            })
        } catch (error) {
            res.json({
                message: error
            })
        }
    });

module.exports = route