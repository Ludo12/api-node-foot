const express = require('express');
const route = express.Router();
const slugify = require('slugify');

const Teams = require('../models/team.models');

const {
    body,
    validationResult
} = require('express-validator');

//création de l'équipe
route.post('/add', //vérification avec le middleware body
    body('email').if(body('email').notEmpty()).isEmail().normalizeEmail().withMessage("Entrer un email valide"),
    body('name').not().isEmpty().trim().escape().withMessage("Le nom de l'équipe doit être renseigné"),
    body('referent').not().isEmpty().trim().escape().withMessage("Le référent de l'équipe doit être renseigné"),
    body('phone').not().isEmpty().trim().escape().isLength(14).withMessage("Renseigner un téléphone"),
    body('paye').if(body('paye').notEmpty()).isBoolean(),
    async (req, res) => {

        try {
            //Array des erreurs
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array()
                });
            }
            //Vérification si le nom de l'équipe est déjà enregistré
            const teamExist = await Teams.find({
                name: req.body.name
            }).countDocuments() > 0 ? true : false;

            //si l'équipe existe on indique une erreur
            if (teamExist) {
                res.status(400).send({
                    message: `L'équipe ${req.body.name} existe déjà`
                });
                return;
            } else { //sinon on crée l'équipe

                const team = new Teams({
                    name: req.body.name,
                    referent: req.body.referent,
                    email: req.body.email,
                    phone: req.body.phone,
                    paye: req.body.paye,
                    slug: slugify(req.body.name, {
                        lower: true
                    }),
                    tournoi_id: req.tournoi
                });

                await team
                    .save(team)
                    .then(data => {
                        res.status(200).send(data);
                    })
                    .catch(err => {
                        res.status(500).send({
                            message: err.message || "Erreur dans la creation de l'equipe"
                        });
                    });

                tournoi = req.tournoi;
                tournoi.nbTeam = await Teams.find().countDocuments();
                await tournoi.save(tournoi);
            };
        } catch (error) {
            res.json({
                message: error
            })
        }
    });

//toutes les équipes
route.get('/all', async (req, res) => {
    try {
        await Teams.find().then(data => res.status(200).send(data)).catch(err => {
            res.status(500).send({
                message: err.message || "Erreur dans la recherche des équipes"
            });
        });
    } catch (error) {
        res.json({
            message: error
        })
    }

});

//update de l'équipe
route.put('/update/:teamSlug', //vérification avec le middleware body
    body('email').if(body('email').notEmpty()).isEmail().normalizeEmail().withMessage("Entrer un email valide"),
    body('name').if(body('name').notEmpty()).trim().escape(),
    body('referent').if(body('referent').notEmpty()).trim().escape(),
    body('phone').if(body('phone').notEmpty()).trim().escape().isLength(14).withMessage("Renseigner un téléphone valide"),
    body('paye').if(body('paye').notEmpty()).isBoolean(),
    async (req, res) => {
        try {
            //Array des erreurs
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array()
                });
            }

            req.body.slug = slugify(req.body.name, {
                lower: true
            });
            req.body.tournoi_id = req.tournoi;
            //on recherche par l'id et on fait les modifications 
            const updatedTeam = await Teams.findOneAndUpdate({
                slug: req.params.teamSlug
            }, req.body, {
                new: true,
                runValidators: true
            });
            res.json({
                updatedTeam
            })
        } catch (error) {
            res.json({
                message: error
            })
        }
    });

//on supprime l'équipe
route.delete('/delete/:teamId', async (req, res) => {
    try {
        //on recherche par l'id et on supprime
        const removedTeam = await Teams.findByIdAndDelete({
            _id: req.params.teamId
        })
        res.json({
            removedTeam
        })
    } catch (error) {
        res.json({
            message: error
        })
    }
});

module.exports = route