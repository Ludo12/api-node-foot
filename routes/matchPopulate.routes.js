const express = require('express');
const route = express.Router();
const slugify = require('slugify');

const Matchs = require('../models/matchPopulate.models');
const Poules = require('../models/poulePopulate.models');
const Resultats = require('../models/resultat.models');

const {
    getMatchs,
    saveMatchs,
    resultats
} = require('../utils/matchsTeamsPopulate');

const {
    body,
    param,
    validationResult
} = require('express-validator');

route.get('/gen/', async (req, res) => {
    try {
        const allTeamInPoule = await Poules
            .find({}, 'name resultats').populate({
                path: "resultats", // populate resultats
                populate: {
                    path: "team", //in resultats, populate team
                    select: "name",
                    model: 'Team'
                }
            });


        let objMatchs = {};

        if (req.tournoi.back) {
            objMatchs['normal'] = getMatchs(allTeamInPoule);
            objMatchs['reversed'] = getMatchs(allTeamInPoule, req.tournoi.back);
        } else {
            objMatchs = getMatchs(allTeamInPoule);
        }
        saveMatchs(objMatchs, req.tournoi.back)

        res.sendStatus(200);
    } catch (error) {
        res.json({
            message: error.message
        })
    };
});

//toutes les matchs
route.get('/all', async (req, res) => {
    await Matchs.find().sort({
        "createdAt": 1
    }).then(data => res.status(200).send(data)).catch(err => {
        res.status(500).send({
            message: err.message || "Erreur dans l'affichage des matchs'"
        });
    });
});

//supprime tous les matchs
route.delete('/deleteall', async (req, res) => {
    await Matchs.deleteMany({})
        .then(data => {
            res.send({
                mesaage: `${data.deletedCount} Matchs ont été supprimés avec succes !`
            });
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Une erreur c'est produite supprimer les Mathcs"
            })
        })
});

//update de l'équipe
route.put('/update/:matchSlug', //vérification avec le middleware body
    body('scoreTeam1').if(body('scoreTeam1').notEmpty()).trim().escape().isNumeric().withMessage('Vous devez entrer un nombre'),
    body('scoreTeam2').if(body('scoreTeam2').notEmpty()).trim().escape().isNumeric().withMessage('Vous devez entrer un nombre'),
    async (req, res) => {
        try {
            //Array des erreurs
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array()
                });
            }

            let pts = {};

            //on cherche le match par le slug
            let teamMatch = await Matchs.findOne({
                    slug: req.params.matchSlug
                }).populate('score.name').then(data => {
                    if (data === 'undefined' || data === null) {
                        res.status(404).send({
                            message: "Le match n'existe pas"
                        })
                    }
                    return data
                })
                .catch(err => {
                    res.status(500).send({
                        message: "Erreur match"
                    })
                });

            //on verifi si le match existe
            if (teamMatch !== (null && 'undefined')) {
                //on attribut les but aux 2 equipes en fonction du scoreTeam1 et scoreTeam2
                teamMatch.score[0]['but'] = Number(req.body.scoreTeam1);
                teamMatch.score[1]['but'] = Number(req.body.scoreTeam2);

                teamMatch.played = true;
                await teamMatch.save()
                    .then((data) => {
                        res.status(200).send({
                            message: 'update scrore with success'
                        })
                    }).catch(err => {
                        res.status(500).send({
                            message: 'error update score'
                        })
                    });

                //on met à jour le resultat pts,score dans la poule
                resultats(teamMatch.pouleName, teamMatch.score[0].name.name, teamMatch.score[1].name.name, req, res);

            }

        } catch (error) {
            res.json({
                message: error.message || "Error match"
            })
        }
    });

//classement
route.get('/resultat',
    async (req, res) => {
        //recupere les 2 premiers de chaque poule pour faire les phases final à 8 équipes
        const poule = await Poules.find({}, 'name');

        let tri = []
        for (let index = 0; index < poule.length; index++) {
            let qualif = []
            let item = await Resultats.classement(poule[index].name)
            item.forEach(el => {
                qualif.push(el.team.name)
            });
            tri.push(qualif)
        }

        let arrQuart = {};
        arrQuart['quart'] = [
            //1er p1 2eme p4
            [tri[0][0], tri[3][1]],
            //1er p2 2eme p3
            [tri[1][0], tri[2][1]],
            //1er p4 2eme p1
            [tri[3][0], tri[0][1]],
            //1er p3 2eme p2
            [tri[2][0], tri[1][1]]
        ];

        saveMatchs(arrQuart)

        try {
            res.json(tri)
        } catch (error) {
            res.json({
                message: error
            })
        }
    });

module.exports = route