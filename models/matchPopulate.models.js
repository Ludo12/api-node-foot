const mongoose = require('mongoose');
const Teams = require('./team.models');

let schema = mongoose.Schema({
    name: String,
    slug: String,
    pouleName: String,
    score: [{
        name: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Team"
        },
        but: {
            type: Number,
            default: 0
        }
    }],
    played: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: true
});

schema.method("toJSON", function () {
    const {
        __v,
        _id,
        ...object
    } = this.toObject();
    object.id = _id;
    return object
});

const MatchPopulate = mongoose.model(
    "MatchPopulate", schema
);

module.exports = MatchPopulate;