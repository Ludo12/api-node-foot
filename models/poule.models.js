const mongoose = require('mongoose');


const pouleSchema = new mongoose.Schema({
    name: String,
    slug: String,
    tournoi_id: {
        type: mongoose.Schema.Types.ObjectId,
        require: true,
        ref: "Tournoi"
    },
    resultats: [{
        name: String,
        pts: {
            type: Number,
            default: 0
        },
        bp: {
            type: Number,
            default: 0
        },
        bc: {
            type: Number,
            default: 0
        },
    }]
    // [{
    //     team: {
    //         type: mongoose.Schema.Types.ObjectId.name,
    //         ref: "Team"
    //     },
    //     res: {
    //         'pts': 0,
    //         'bp': 0,
    //         'bc': 0
    //     }
    // }]
});


const Poule = mongoose.model(
    "Poule", pouleSchema);

module.exports = Poule;