const mongoose = require('mongoose');

let schema = mongoose.Schema({
    name: String,
    pouleName: String,
    score: [{
        name: String,
        but: {
            type: Number,
            default: 0
        }
    }],
    resultat: [{
        name: String,
        pts: {
            type: Number,
            default: 0
        }
    }],
    slug: String,
    team1: String,
    team2: String,
    played: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: true
});

schema.method("toJSON", function () {
    const {
        __v,
        _id,
        ...object
    } = this.toObject();
    object.id = _id;
    return object
})

const Match = mongoose.model(
    "Match", schema
);

module.exports = Match;