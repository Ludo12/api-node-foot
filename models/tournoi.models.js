const mongoose = require('mongoose');

const Tournoi = mongoose.model(
    "Tournoi",
    new mongoose.Schema({
        name: String,
        date: {
            type: Date,
            default: Date.now()
        },
        nbPoule: Number,
        nbTeam: Number,
        slug: String,
        back: {
            type: Boolean,
            default: false
        }
    }));

module.exports = Tournoi;