const mongoose = require('mongoose');


const resultatSchema = new mongoose.Schema({
    poule: String,
    tournoi_id: {
        type: mongoose.Schema.Types.ObjectId,
        require: true,
        ref: "Tournoi"
    },
    team: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Team"
    },
    pts: {
        type: Number,
        default: 0
    },
    bp: {
        type: Number,
        default: 0
    },
    bc: {
        type: Number,
        default: 0
    }
});

resultatSchema.statics.classement = function (namePoule, callback) {
    // const namePoule = slug.replace('-', ' ');
    return this.find({
        poule: namePoule
    }, callback).populate({
        path: 'team',
        select: 'name'
    }).sort({
        'poule': 1,
        'pts': -1,
        'bp': -1,
        'bc': 1
    }).limit(2);
}

const Resultat = mongoose.model(
    "Resultat", resultatSchema);

module.exports = Resultat;