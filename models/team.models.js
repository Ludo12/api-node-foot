const mongoose = require('mongoose');

const Team = mongoose.model(
    "Team",
    new mongoose.Schema({
        name: String,
        referent: String,
        email: {
            type: String,
            default: ""
        },
        phone: {
            type: String,
            default: "Exemple 00-00-00-00-00"
        },
        paye: {
            type: Boolean,
            default: false,
        },
        slug: String,
        tournoi_id: {
            type: mongoose.Schema.Types.ObjectId,
            require: true,
            ref: "Tournoi"
        },
    }));

module.exports = Team;