const mongoose = require('mongoose');


const pouleSchema = new mongoose.Schema({
    name: String,
    slug: String,
    tournoi_id: {
        type: mongoose.Schema.Types.ObjectId,
        require: true,
        ref: "Tournoi"
    },
    resultats: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Resultat"
    }]
});


const PoulePopulate = mongoose.model(
    "PoulePopulate", pouleSchema);

module.exports = PoulePopulate;