const express = require('express');
const cors = require("cors");
const connectDB = require('./config/db.conf');

const tournoisRoute = require('./routes/tournoi.routes');
const poulesRoute = require('./routes/poule.routes');
const teamsRoute = require('./routes/team.routes');
const matchsRoute = require('./routes/match.routes');
const poulesRoutePopulate = require('./routes/poulePopulate.routes');
const matchsRoutePopulate = require('./routes/matchPopulate.routes');

const PORT = 8080;

const app = express();

let corsOptions = {
    origin: "http://localhost:8081"
};

const Tournois = require('./models/tournoi.models');
app.param('slugTournoi', async (req, res, next, slugTournoi) => {
    slugTournoi = escape(slugTournoi.trim());
    await Tournois.findOne({
        slug: slugTournoi
    }, function (error, tournoi) {
        if (error) return next(error);
        if (!tournoi) return next(new Error('Nothing is found'));
        req.tournoi = tournoi;
        next();
    });
});

app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));

app.use('/api/tournoi/', tournoisRoute);
app.use('/api/:slugTournoi/poule', poulesRoutePopulate);
app.use('/api/:slugTournoi/team/', teamsRoute);
app.use('/api/:slugTournoi/match/', matchsRoutePopulate);

app.get("/", (req, res) => {
    res.json({
        message: "Bienvenue"
    })
});

try {
    connectDB();
    app.listen(PORT, () => {
        console.log(`Listening on ${PORT}`);
    });
} catch (error) {
    console.log(error)
}